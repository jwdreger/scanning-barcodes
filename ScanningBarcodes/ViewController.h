//
//  ViewController.h
//  ScanningBarcodes
//
//  Created by James Dreger on 4/21/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AVFoundation/AVFoundation.h>

@interface ViewController : UIViewController <AVCaptureMetadataOutputObjectsDelegate>

@property (weak, nonatomic) IBOutlet UIView *scanPreview;
@property (weak, nonatomic) IBOutlet UILabel *labelStatus;
@property (weak, nonatomic) IBOutlet UILabel *labelResult;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *buttonStart;

- (IBAction)startStopScanning:(id)sender;

@end
