//
//  ViewController.m
//  ScanningBarcodes
//
//  Created by James Dreger on 4/21/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@property (nonatomic) BOOL scanning;
@property (nonatomic, strong) AVCaptureSession *captureSession;
@property (nonatomic, strong) AVCaptureVideoPreviewLayer *videoPreviewLayer;
@property (nonatomic, strong) AVAudioPlayer *audioPlayer;
@property (nonatomic) NSArray *barCodeTypes;

- (BOOL)startScanning;
- (void)stopScanning;
- (void)beep;

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    _scanning = NO;
    _captureSession = nil;
    
    // Set Barcode Types
    _barCodeTypes = @[AVMetadataObjectTypeUPCECode, AVMetadataObjectTypeCode39Code,AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeQRCode];
    
    [self beep];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)startStopScanning:(id)sender
{
}

- (BOOL)startScanning
{
    return YES;
}

- (void)stopScanning
{
    
}

- (void)beep
{
    
}
@end
