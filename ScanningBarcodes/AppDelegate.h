//
//  AppDelegate.h
//  ScanningBarcodes
//
//  Created by James Dreger on 4/21/14.
//  Copyright (c) 2014 James Dreger. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
